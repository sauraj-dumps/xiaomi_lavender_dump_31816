## xdroid_lavender-userdebug 11 RKQ1.210922.001 eng.xyzuan.20211007.074003 release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: lavender
- Brand: Xiaomi
- Flavor: xdroid_lavender-userdebug
- Release Version: 11
- Id: RKQ1.210922.001
- Incremental: eng.xyzuan.20211007.074003
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:11/RQ3A.210905.001/7511028:user/release-keys
- OTA version: 
- Branch: xdroid_lavender-userdebug-11-RKQ1.210922.001-eng.xyzuan.20211007.074003-release-keys
- Repo: xiaomi_lavender_dump_31816


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
